
# Fier Language                                           [![Build Status](https://travis-ci.org/Scottlr/fier-lang.svg?branch=master)](https://travis-ci.org/Scottlr/fier-lang) [![Coverage Status](https://coveralls.io/repos/github/Scottlr/fier-lang/badge.svg?branch=master)](https://coveralls.io/github/Scottlr/fier-lang?branch=master)

Educational project on compiler writing using the Rust programming language. Will eventually compile down to MSIL maybe native, depends on how much research I can manage to fit in :) Syntax will be a hybrid between Rust and C#. This is a relatively naive implementation.

## Current Progress
Currently trying to extract out tokens from source code. 

## Planned Features
- async/await
- Options, no nulls
- Tuples inbuilt
- Rust style error/exception handling
- Pattern matching
- Ownership/borrowing/lifetimes?
- Syntax tree visualiser
